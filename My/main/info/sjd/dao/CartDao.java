package info.sjd.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import info.sjd.entity.Cart;

public class CartDao {
	public static void create(Cart cart) {
		String sql = "insert into carts (user_id, creationTime, is_closed) values (?,?,?,?,?)";

		try (Connection connection = ConnectionFactory.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
			preparedStatement.setInt(1, cart.getUserId());
			preparedStatement.setLong(2, cart.getCreationTime());
			preparedStatement.setBoolean(4, cart.getIsClosed());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public static void update(Cart cart) {
		String sql = "update carts set user_id = ?, creationTime = ?, cart_id = ? where cart_id = ?";

		try (Connection connection = ConnectionFactory.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(sql)) {

			preparedStatement.setInt(1, cart.getUserId());
			preparedStatement.setLong(2, cart.getCreationTime());
			preparedStatement.setBoolean(4, cart.getIsClosed());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static Cart findById(Integer cartId) {
		String sql = "select * from carts where  cart_id = ?";

		try (Connection connection = ConnectionFactory.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
			preparedStatement.setInt(1, cartId);
			ResultSet rs = preparedStatement.executeQuery();
			if (rs.next()) {
				Integer userId = rs.getInt("user_id");
				Long creationTime = rs.getLong("creationTime");
				boolean isClosed = rs.getBoolean("cart_id");
				return new Cart(cartId, userId, creationTime, isClosed);
			} else {
				return null;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void delete(Integer cartId) {
		String sql = "delete from carts where cart_id = ?";

		try (Connection connection = ConnectionFactory.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
			preparedStatement.setInt(1, cartId);
			preparedStatement.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}

