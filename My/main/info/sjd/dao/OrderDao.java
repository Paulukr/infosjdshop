package info.sjd.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import info.sjd.entity.Order;

public class OrderDao {
	public static void create(Order order) {
		String sql = "insert into users (item_id, amount, cart_id) values (?,?,?,?,?)";

		try (Connection connection = ConnectionFactory.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
			preparedStatement.setInt(1, order.getItemId());
			preparedStatement.setInt(2, order.getAmount());
			preparedStatement.setInt(4, order.getCartId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public static void update(Order order) {
		String sql = "update users set item_id = ?, amount = ?, cart_id = ? where oder_id = ?";

		try (Connection connection = ConnectionFactory.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(sql)) {

			preparedStatement.setInt(1, order.getItemId());
			preparedStatement.setInt(2, order.getAmount());
			preparedStatement.setInt(4, order.getCartId());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static Order findById(Integer orderId) {
		String sql = "select * from orders where  order_id = ?";

		try (Connection connection = ConnectionFactory.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
			preparedStatement.setInt(1, orderId);
			ResultSet rs = preparedStatement.executeQuery();
			if (rs.next()) {
				Integer itemId = rs.getInt("item_id");
				Integer amount = rs.getInt("amount");
				Integer cartId = rs.getInt("cart_id");
				return new Order(orderId, itemId, amount, cartId);
			} else {
				return null;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void delete(Integer orderId) {
		String sql = "delete from orders where order_id = ?";

		try (Connection connection = ConnectionFactory.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
			preparedStatement.setInt(1, orderId);
			preparedStatement.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}

