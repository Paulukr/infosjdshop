package info.sjd.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import info.sjd.entity.Item;

public class ItemDao {// 5432

	public static void create(Item item) {
		String sql = "insert into items (item_id, name, prices) values (?,?,?)";

		try (Connection connection = ConnectionFactory.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
			preparedStatement.setInt(1, item.getItemId());
			preparedStatement.setString(2, item.getName());
			preparedStatement.setInt(3, item.getPrice());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public static void update(Item item) {
		String sql = "update items set name = ?, price= ? where item_id = ?";

		try (Connection connection = ConnectionFactory.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(sql)) {

			preparedStatement.setString(1, item.getName());
			preparedStatement.setInt(2, item.getPrice());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static Item findById(Integer itemId) {
		String sql = "select * from items where item_id = ?";

		try (Connection connection = ConnectionFactory.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
			preparedStatement.setInt(1, itemId);
			ResultSet rs = preparedStatement.executeQuery();
			if (rs.next()) {
				String name = rs.getString("user_password");
				Integer price = rs.getInt("user_id");
				
				return new Item(itemId, name, price);
			} else {
				return null;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void delete(Integer item_id) {
		String sql = "delete from items where itemId = ?";

		try (Connection connection = ConnectionFactory.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
			preparedStatement.setInt(1, item_id);
			preparedStatement.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
