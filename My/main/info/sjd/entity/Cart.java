package info.sjd.entity;

public class Cart {
		private Integer cartId;
		private Integer userId;
		private Long creationTime;
		private Boolean isClosed;
		public Integer getCartId() {
			return cartId;
		}
		public void setCartId(Integer cartId) {
			this.cartId = cartId;
		}
		public Integer getUserId() {
			return userId;
		}
		public void setUserId(Integer userId) {
			this.userId = userId;
		}
		public Long getCreationTime() {
			return creationTime;
		}
		public void setCreationTime(Long creationTime) {
			this.creationTime = creationTime;
		}
		public Boolean getIsClosed() {
			return isClosed;
		}
		public void setIsClosed(Boolean isClosed) {
			this.isClosed = isClosed;
		}
		
		public Cart(Integer cartId, Integer userId, Long creationTime, Boolean isClosed) {
			this.cartId = cartId;
			this.userId = userId;
			this.creationTime = creationTime;
			this.isClosed = isClosed;
		}
		
		public Cart() {
		}
		
}
