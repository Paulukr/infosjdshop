package info.sjd.entity;

public class Order {
	
	private Integer orederId;
	private Integer itemId;
	private Integer amount;
	private Integer cartId;
	
	public Integer getOrederId() {
		return orederId;
	}
	public void setOrederId(Integer orederId) {
		this.orederId = orederId;
	}
	public Integer getItemId() {
		return itemId;
	}
	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}
	public Integer getAmount() {
		return amount;
	}
	public void setAmount(Integer amount) {
		this.amount = amount;
	}
	public Integer getCartId() {
		return cartId;
	}
	public void setCartId(Integer cartId) {
		this.cartId = cartId;
	}
	public Order(Integer orederId, Integer itemId, Integer amount, Integer cartId) {
		this.orederId = orederId;
		this.itemId = itemId;
		this.amount = amount;
		this.cartId = cartId;
	}
	
	public Order() {
	}
	
}
