package info.sjd.entity;

public class User {
	private Integer userId;
	private String login;
	private String password;
	private String firstName;
	private String secondName;
	
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getSecondName() {
		return secondName;
	}
	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}
	
	public User(Integer userId, String login, String password, String firstName, String secondName) {
		super();
		this.userId = userId;
		this.login = login;
		this.password = password;
		this.firstName = firstName;
		this.secondName = secondName;
	}

	public User() {
	}

	
}
