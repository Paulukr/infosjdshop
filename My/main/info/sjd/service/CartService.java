package info.sjd.service;

import info.sjd.dao.CartDao;
import info.sjd.entity.Cart;

public class CartService {

	public static void createOrUpdate(Cart cart) {
		Cart checkCart = CartDao.findById(cart.getCartId());
		if (checkCart == null) {
			CartDao.create(cart);
		}else {
			CartDao.update(cart);
		}
	}
	
	public static Cart findByLogin(Integer orderId) {
		return CartDao.findById(orderId);
	}
	
	public static void delete(Integer orderId) {
		CartDao.delete(orderId);
	}
}
