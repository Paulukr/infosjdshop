package info.sjd.service;

import info.sjd.dao.OrderDao;
import info.sjd.entity.Order;

public class OrderService {

	public static void createOrUpdate(Order order) {
		Order checkOrder = OrderDao.findById(order.getOrederId());
		if (checkOrder == null) {
			OrderDao.create(order);
		}else {
			OrderDao.update(order);
		}
	}
	
	public static Order findByLogin(Integer orderId) {
		return OrderDao.findById(orderId);
	}
	
	public static void delete(Integer orderId) {
		OrderDao.delete(orderId);
	}
}
