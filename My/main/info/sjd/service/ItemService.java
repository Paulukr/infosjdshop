package info.sjd.service;

import info.sjd.dao.ItemDao;
import info.sjd.entity.Item;

public class ItemService {

	public static void createOrUpdate(Item Item) {
		Item checkItem = ItemDao.findById(Item.getItemId());
		if (checkItem == null) {
			ItemDao.create(Item);
		}else {
			ItemDao.update(Item);
		}
	}
	
	public static Item findByLogin(Integer ItemId) {
		return ItemDao.findById(ItemId);
	}
	
	public static void delete(Integer ItemId) {
		ItemDao.delete(ItemId);
	}
}
