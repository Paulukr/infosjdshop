package info.sjd.service;

import info.sjd.dao.UserDao;
import info.sjd.entity.User;

public class UserService {

	public static void createOrUpdate(User user) {
		User checkUser = UserDao.findByLogin(user.getLogin());
		if (checkUser == null) {
			UserDao.create(user);
		}else {
			UserDao.update(user);
		}
	}
	
	public static User findByLogin(String login) {
		return UserDao.findByLogin(login);
	}
	
	public static void delete(String login) {
		UserDao.delete(login);
	}
}
