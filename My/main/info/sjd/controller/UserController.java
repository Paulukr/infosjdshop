package info.sjd.controller;

import java.io.IOException;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import info.sjd.dao.UserDao;
import info.sjd.entity.User;
import info.sjd.service.UserService;

@WebServlet("/user")
@SuppressWarnings("serial")
public class UserController extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String login = request.getParameter("login");
		User user = UserDao.findByLogin(login);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	}

	protected void doPut(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String login = request.getParameter("login");
		String password = request.getParameter("login");
		String firstName = request.getParameter("login");
		String lastName = request.getParameter("login");
		Integer userId = UUID.randomUUID().hashCode();
		User user = new User(userId, login, password, firstName, lastName);
		UserService.createOrUpdate(user);
	}
	
	protected void doPatch(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String login = request.getParameter("login");
		String password = request.getParameter("login");
		String firstName = request.getParameter("login");
		String lastName = request.getParameter("login");
		Integer userId = UUID.randomUUID().hashCode();
		User user = new User(userId, login, password, firstName, lastName);
		UserService.createOrUpdate(user);
	}

	protected void doDelete(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String login = request.getParameter("login");
		UserService.delete(login);
	}

}
