package info.sjd.service;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import info.sjd.entity.User;

public class UserServiceTest {
	UserService userService;
	@Before
	public void setUp() throws Exception {
		userService = new UserService();
	}

	@Test
	public void testCreateOrUpdate() {
		User userIn = new User(1, "tl", "tp", "tf", "ts");
		UserService.createOrUpdate(userIn);
		User userOut = UserService.findByLogin(userIn.getLogin());
		assertEquals(userIn.getFirstName(), userOut.getFirstName());
		
		userIn.setFirstName(userIn.getFirstName() + "1");
		UserService.createOrUpdate(userIn);
		userOut = UserService.findByLogin(userIn.getLogin());
		assertEquals(userIn.getFirstName(), userOut.getFirstName());
		
		UserService.delete(userIn.getLogin());
		assertNull(UserService.findByLogin(userIn.getLogin()));
	}

	@Test
	public void testFindByLogin() {
//		fail("Not yet implemented");
	}

	@Test
	public void testDelete() {
//		fail("Not yet implemented");
	}

}
